<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Basket\Session;

use PIPEU\Factura\Domain\Interfaces\InterfaceDelivery;
use PIPEU\Factura\Domain\Model\Documents\Order;

/**
 * Class InterfaceStorage
 *
 * @package PIPEU\Factura\Basket\Session
 */
interface InterfaceStorage {

	/**
	 * @return Order
	 */
	public function getOrder();

	/**
	 * @return InterfaceDelivery
	 */
	public function getDelivery();

	/**
	 * @return void
	 */
	public function prune();

}
