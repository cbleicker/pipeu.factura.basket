<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Basket\Session;

use PIPEU\Factura\Domain\Interfaces\InterfaceDelivery;
use PIPEU\Factura\Domain\Interfaces\InterfaceFacturaItem;
use PIPEU\Factura\Domain\Model\Documents\Order;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class Storage
 *
 * @package PIPEU\Factura\Basket\Session
 * @Flow\Scope("session")
 */
class Storage implements InterfaceStorage{

	/**
	 * @var Order
	 */
	protected $order;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->order = new Order();
	}

	/**
	 * @return Order
	 * @Flow\Session(autoStart = TRUE)
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * @return InterfaceDelivery
	 */
	public function getDelivery(){
		return $this->getOrder()->getFacturaItems()->filter($this->getDeliveryFilter())->first();
	}

	/**
	 * @return callable
	 */
	public static function getDeliveryFilter(){
		return function(InterfaceFacturaItem $facturaItem){
			return $facturaItem instanceof InterfaceDelivery;
		};
	}

	/**
	 * @return callable
	 */
	public static function getNoDeliveryFilter(){
		return function(InterfaceFacturaItem $facturaItem){
			return !$facturaItem instanceof InterfaceDelivery;
		};
	}

	/**
	 * @return integer
	 */
	public function getTotalNumberOfItems(){
		return $this->getOrder()->getFacturaItems()->filter(self::getNoDeliveryFilter())->count();
	}

	/**
	 * @return mixed
	 */
	public function prune() {
		$this->order = new Order();
	}
}
