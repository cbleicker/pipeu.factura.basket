<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Factura\Basket\Property\TypeConverter;

use PIPEU\Factura\Basket\Session\Storage;
use PIPEU\Factura\Delivery\Domain\Model\Provider\Dummy;
use PIPEU\Factura\Domain\Interfaces\InterfaceDelivery;
use TYPO3\Flow\Property\TypeConverter\AbstractTypeConverter;
use TYPO3\Flow\Property\PropertyMappingConfigurationInterface;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class DeliveryConverter
 *
 * @package PIPEU\Factura\Basket\Property\TypeConverter
 * @Flow\Scope("singleton")
 */
class DeliveryConverter extends AbstractTypeConverter {

	/**
	 * @var Storage
	 * @Flow\Inject
	 */
	protected $basket;

	/**
	 * @var array<string>
	 */
	protected $sourceTypes = array('string');

	/**
	 * @var string
	 */
	protected $targetType = 'PIPEU\Factura\Domain\Interfaces\InterfaceDelivery';

	/**
	 * @var integer
	 */
	protected $priority = 1;

	/**
	 * @param string $source
	 * @param string $targetType
	 * @param array $convertedChildProperties
	 * @param PropertyMappingConfigurationInterface $configuration
	 * @return InterfaceDelivery
	 */
	public function convertFrom($source, $targetType, array $convertedChildProperties = array(), PropertyMappingConfigurationInterface $configuration = NULL) {
		/** @var InterfaceDelivery $delivery */
		$delivery = $this->basket->getOrder()->getPossibleDeliveries()->filter($this->getDeliveryByTypeFilter($source))->first();
		if($delivery instanceof InterfaceDelivery){
			return $delivery;
		}else{
			return new Dummy();
		}
	}

	/**
	 * @param string $type
	 * @return callable
	 */
	protected function getDeliveryByTypeFilter($type) {
		return function (InterfaceDelivery $delivery) use ($type) {
			return $delivery->getType() === $type;
		};
	}
}
